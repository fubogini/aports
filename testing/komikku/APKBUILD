# Contributor: Jakob Meier <comcloudway@ccw.icu>
# Maintainer: Jakob Meier <comcloudway@ccw.icu>
pkgname=komikku
pkgver=1.30.0
pkgrel=0
pkgdesc="manga reader for GNOME"
url="https://gitlab.com/valos/Komikku"
arch="noarch !s390x" # limited by blueprint-compiler
license="GPL-3.0-only"
depends="
	libadwaita
	py3-beautifulsoup4
	py3-brotli
	py3-colorthief
	py3-dateparser
	py3-emoji
	py3-gobject3
	py3-keyring
	py3-lxml
	py3-magic
	py3-natsort
	py3-piexif
	py3-pillow
	py3-pure_protobuf
	py3-rarfile
	py3-requests
	py3-unidecode
	webkit2gtk-6.0
	"
makedepends="
	blueprint-compiler-dev
	cmake
	desktop-file-utils
	gobject-introspection-dev
	gtk4.0-dev
	libadwaita-dev
	meson
	"
subpackages="$pkgname-lang $pkgname-pyc"
source="https://gitlab.com/valos/Komikku/-/archive/v$pkgver/Komikku-v$pkgver.tar.gz"
builddir="$srcdir/Komikku-v$pkgver"

build() {
	abuild-meson build

	ninja -C build
}

check() {
	meson test -C build --print-errorlog
}

package() {
	DESTDIR="$pkgdir" meson install -C build
}

sha512sums="
1555aa8755f20a6ef2c551c7e515a1e50f0b3d6692dc5516a420e0c0ada4adc3b855cca37c67c0fe1b631f5b047597260fd2478e4d93014e053e5c8deeb4cc79  Komikku-v1.30.0.tar.gz
"
